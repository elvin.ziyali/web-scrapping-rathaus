package de.rathouse.scrapper.controller;

import de.rathouse.scrapper.service.ScrapingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class MainController {
    @Value("${scraping.url}")
    private String url;
    private final ScrapingService scrapingService;

    public MainController(ScrapingService scrapingService) {
        this.scrapingService = scrapingService;
    }

    @GetMapping
    public Integer getNumber(){
        try {
            return scrapingService.getNumber(url);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

}
