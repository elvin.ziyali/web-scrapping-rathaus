package de.rathouse.scrapper.service;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;

@Service
public class ScrapingService {
    private final JavaMailSender mailSender;
    @Value("${scraping.url}")
    private String link;
    @Value("${scraping.max-date}")
    private Integer maxDate;

    public ScrapingService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public Integer getNumber(String link) throws IOException {
        Connection.Response res = Jsoup.connect(link).method(Connection.Method.GET).execute();
        WebClient webClient = new WebClient();
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getCookieManager().setCookiesEnabled(true);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        // Wait time
        webClient.waitForBackgroundJavaScript(6000);
        webClient.getOptions().setThrowExceptionOnScriptError(false);

        URL url = new URL(this.link);
        WebRequest requestSettings = new WebRequest(url, HttpMethod.GET);
        HtmlPage page = webClient.getPage(requestSettings);

        synchronized (page) {
            try {
                page.wait(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Document doc = Jsoup.parse(page.asXml());
        Elements nextap_red = doc.getElementsByClass("nextap red");
        System.err.println(nextap_red.toString());
        String first = nextap_red.get(0).toString();
        String[] split = first.split(":");
        String[] str = split[1].split(" ");
        int i = -1;
        try {
            i = Integer.parseInt(str[2]);
        } catch (Exception ex) {
            if (str[2].equalsIgnoreCase("heute")) {
                i = 1;
            } else if (str[2].equalsIgnoreCase("morgen")) {
                i = 2;
            }
        }
        return i;

    }

    public void sendMail(Integer number) {
        String to = "e.ismatli@gmail.com";
        String subject = "Rathaus Registration Day";
        String text = "You can find the number of days here : " + number
                +"\n link:\n https://www.terminland.de/StadtSiegen/default.aspx?m=29700&ll=UvJfW&dpp=0&dlgid=1708834605&step=1&dlg=1&a1572549576=1579575228&css=1&goto#step10\n";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@rathaus.siegen2022.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }

    @Scheduled(fixedDelayString = "${scraping.delay}")
    public void scheduledTask() throws IOException {
        Integer number = this.getNumber(this.link);
        System.err.println(number);
        if (number <= this.maxDate && number >= 0)
            this.sendMail(number);
    }

}
